import { Component, OnInit } from '@angular/core';
import { LoginService, AuthService, ErrorHttpHandler, } from '../login.service';
import { Router } from '@angular/router';
import {trigAnimation} from '../../shared/animation';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ILoginInfos } from '../model/loginInfos';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [trigAnimation]
})
export class ProfileComponent implements OnInit {
  Email = new FormControl('', [Validators.required, Validators.email]);
  public resetEmail: boolean = false;
  public resetPwd: boolean = false;
  public connected: boolean = false;
  public email: string = "";
  public password: string = "";
  public newInfos!: ILoginInfos;
  isOpen = true;
  hide = true;
  constructor(public _loginService: LoginService, private _router: Router, private authHandle: AuthService, public snackBar: MatSnackBar, private _apiservice: ApiService, private errorHttpHandler: ErrorHttpHandler,) {
  }

  ngOnInit(): void {
    this.updateLoginInfos();
  }

  getErrorEmailMessage() {
    if (this.Email.hasError('required')) {
      return 'Entrer un Email*';
    }

    return this.Email.hasError('email') ? '' : '';
  }
  getErrorMessagePassword() {
    if (this.password.length <= 3 && this.password.length !== 0) {
      return 'hello';
    }
    return ''
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  connect() {
     if (this.email !== "" && this.password !== "") {
    /* if (this.email.includes('@') && (this.email.includes(".com") || this.email.includes(".fr") || this.email.includes(".en"))) {

    if (this._loginService.login(this.email, this.password) !== null) {*/
        this._apiservice.login(this.email, this.password).subscribe({
          next: (res: any) => {
            this._apiservice.setToken(res.access_token);
            console.log(this._apiservice.token);
            console.log("connected");
            
            //this._router.navigate(['/profile']);
          },
          error: (error) => {
              console.warn(error);
          },
          complete: () => {
            
            this._apiservice.get().subscribe({
              next: (res: any) => {
                this._loginService.modifyLoginInfos(res);
                this._loginService.modifyEmail(this.email);
                this._loginService.modifyPwd(this.password);
                this.connected = true;
                this._loginService.loginInfos.connected = true;
                console.log(this._loginService.getLoginInfos());
              }
            });
          }
        })};
    }

  disconnect() {
    this._loginService.disconnect();
    this.updateLoginInfos();
  }

  updateLoginInfos() {
    let loginInfos = this._loginService.getLoginInfos();
    this.connected = loginInfos.connected;
    this.email = loginInfos.email;
    this.password = loginInfos.password;
  }

  toggleResetEmail() {
    this.resetEmail = !this.resetEmail;
  }

  /*modifyEmail(email: string) {
    this._loginService.modifyEmail(email);
    this.updateLoginInfos();
    this.resetEmail = false;
  }*/
  modifyMail(email: any) {
    this._apiservice.modifyEmail(email).subscribe({
      next: (res: any)=> {
        this.email = email;
        console.log(res);
      }
    });

  }

  toggleResetPwd() {
    this.resetPwd = !this.resetPwd;
  }

  modifyPwd(password: any) {
    this._apiservice.modifyPassword(password).subscribe({
      next: (res: any) => {
        this.password = password;
        console.log(res);
      }
    });
  }
}

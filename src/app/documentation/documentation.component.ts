import { Component, OnInit } from '@angular/core';
import {trigAnimation} from "../../shared/animation";

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss'],
  animations: [trigAnimation]
})
export class DocumentationComponent implements OnInit {
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
  }

  downloadFile(){
    let link = document.createElement("a");
    link.download = "2023_UD1_Superficia";
    link.href = "assets/downloads/2023_UD1_Superficia.pdf";
    link.click();
}

}

import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { ApiService } from '../api.service';
import {trigAnimation} from '../../shared/animation';

@Component({
  selector: 'app-download-beta',
  templateUrl: './download-beta.component.html',
  styleUrls: ['./download-beta.component.scss'],
  animations: [trigAnimation]
})
export class DownloadBetaComponent implements OnInit {

  public betaAccess: boolean = false;
  public connected: boolean = false;
  isOpen = true;

  constructor(private _loginService: LoginService, private _apiService: ApiService) { }

  ngOnInit(): void {
    let infos = this._loginService.getLoginInfos();
    this.betaAccess = infos.betaAccess;
    this.connected = infos.connected;
  }

  downloadFile(){
    let link = document.createElement("a");
    link.download = "SuperficiaSetup.exe";
    link.href = "assets/downloads/SuperficiaSetup.exe";
    link.click();

}

}

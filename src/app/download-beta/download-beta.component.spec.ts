import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadBetaComponent } from './download-beta.component';

describe('DownloadBetaComponent', () => {
  let component: DownloadBetaComponent;
  let fixture: ComponentFixture<DownloadBetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadBetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadBetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

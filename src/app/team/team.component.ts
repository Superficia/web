import { Component, OnInit } from '@angular/core';
import { members,IMembers } from '../model/members';
import {trigeAnimation} from '../../shared/animation';


@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  animations : [trigeAnimation]
})
export class TeamComponent implements OnInit {
  isOpen = true;

  public members: IMembers[] = [];

  constructor() { }

  ngOnInit(): void {
    this.members = members;
  }
}

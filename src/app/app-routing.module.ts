import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetaClosedComponent } from './beta-closed/beta-closed.component';
import { DownloadBetaComponent } from './download-beta/download-beta.component';
import { EnterBetaCodeComponent } from './enter-beta-code/enter-beta-code.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ProjectComponent } from './project/project.component';
import { RoadmapComponent } from './roadmap/roadmap.component';
import { TeamComponent } from './team/team.component';
import { ContactUsViewComponent } from "./contact-us/contact-us-view.component";
import { DocumentationComponent } from './documentation/documentation.component';
import {RegisterViewComponent} from "./register/register-view.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'enterbetacode', component: EnterBetaCodeComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'team', component: TeamComponent},
  {path: 'roadmap', component: RoadmapComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'betaclosed', component: BetaClosedComponent},
  {path: 'downloadbeta', component: DownloadBetaComponent},
  {path: 'home', component:HomeComponent},
  {path: 'contactus', component: ContactUsViewComponent},
  {path: 'documentation', component: DocumentationComponent},
  {path: 'register', component: RegisterViewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [EnterBetaCodeComponent, ProfileComponent, TeamComponent, RoadmapComponent, ProjectComponent, BetaClosedComponent, DownloadBetaComponent, ContactUsViewComponent, HomeComponent, DocumentationComponent, RegisterViewComponent]

import { Component, OnInit } from '@angular/core';
import {trigeAnimation} from '../../shared/animation';

export interface RoadmapEvent {
  name: string;
  description: string;
  date: string;
}

@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.scss'],
  animations: [trigeAnimation]
})
export class RoadmapComponent implements OnInit {

  isOpen = true;
  public displayedColumns: string[] = ["name", "description", "date"];
  public dataSource = [
    {name: "Creation", description: "Creation du projet superficia :", date:"04/09/20"},
    {name: "Adhésion", description: "Approbation du projet par des professionnels :", date:"07/09/20"},
    {name: "Idéation", description: "Création de contenu pour illustrer le projet :", date:"13/12/20"},
    {name: "Dév", description: "Début de la phase de développement :", date:"04/21"},
    {name: "Algo", description: "2 membres nous ont rejoint pour travailler sur l'algorithme :", date: "04/21"},
    {name: "Unity", description: "Changement de imgui à unity pour le client :", date:"11/21"},
    {name: "Alpha", description: "Première sortie de l'alpha :", date:"01/22"},
    {name: "Site", description: "Le site superficia est en ligne :", date: "04/22"},
    {name: "Serveur", description: "Mise en ligne du serveur :", date: "04/22"},
    {name: "Bêta", description: "Sortie de la bêta :", date:"05/22"},
    {name: "Test", description: "Phase bêta testeur :", date: "06/22"},
    {name: "Visuel", description: "Refonte visuel du site :", date:"09/22"},
    {name: "2d/3d", description: "Edition de plan 2d/3d :", date:"11/22"},
    {name: "Release", description: "Release finale :", date:"12/22"},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}

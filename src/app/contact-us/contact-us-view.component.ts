import { Component, OnInit } from '@angular/core';
import { trigAnimation } from '../../shared/animation';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import { ApiService } from '../api.service';
import {LoginService} from "../login.service";

@Component({
    selector: 'app-contact-us-view',
    templateUrl: './contact-us-view.component.html',
    styleUrls: ['./contact-us-view.component.scss'],
    animations: [trigAnimation],
})
export class ContactUsViewComponent implements OnInit {
    isOpen = true;
    public email: string = '';
    public content: string = '';
    public subject: string = '';

    constructor( private _location: Location, private _apiservice: ApiService, public _loginService: LoginService
    ) {
    }
    public onClickModify(): void {
        console.log(this.subject, this.content, this.email)
          if (this.subject !== '' && this.email !== '' && this.content) {
              this._apiservice.contactUs(this.subject, this.email, this.content).subscribe({
              next: (res: any) => {
                  console.log(res);
              },
                  error: (error) => {
                  console.warn(error);
                  },
          })};
    }
    backClicked(): void {
        this._location.back();
    }

    ngOnInit(): void {
        let infos = this._loginService.getLoginInfos();
        this.content = infos.content;
        this.subject = infos.subject;
        this.email = infos.email;
    }

    public toogleiSOpen(): void {
        this.isOpen = !this.isOpen;
    }
}

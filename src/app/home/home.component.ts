import { Component, OnInit } from '@angular/core';
import { ConfigLoaderService } from '../config-loader.service';
import {trigAnimation} from '../../shared/animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [trigAnimation]
})
export class HomeComponent implements OnInit {

  public betaOpen: boolean = false;
  public enterBetaCodeUrl: string = "";
  public profileUrl: string ="";
  public downloadBetaUrl: string ="";
  isOpen = true;
  constructor(private _configLoaderService: ConfigLoaderService) { }

  ngOnInit(): void {
    this.betaOpen = this._configLoaderService.isBetaOpen();
    if (this.betaOpen) {
      this.enterBetaCodeUrl = "/enterbetacode";
      this.profileUrl = "/profile";
      this.downloadBetaUrl = "/downloadbeta"
    } else {
      this.enterBetaCodeUrl = "/betaClosed";
      this.profileUrl = "/betaClosed";
      this.downloadBetaUrl = "/betaClosed"
    }
  }
}

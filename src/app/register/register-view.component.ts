import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApiService} from "../api.service";
import {trigAnimation} from '../../shared/animation';
import {FormControl, Validators} from "@angular/forms";
import {MessageApiResponse} from "../ResponsesMsgCodes";
import {IRegister} from "../model/loginInfos";
import {LoginService} from "../login.service";

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss'],
  animations: [trigAnimation]
})
export class RegisterViewComponent implements OnInit {
  Email = new FormControl('', [Validators.required, Validators.email]);
  public phone: string = '';
  public username: string = '';
  public email: string = '';
  public password: string = '';
  public connected: boolean = false;
  isOpen = true;


  constructor(
    private userController: ApiService,
    private router: Router,
    public _loginService: LoginService,
  ) {}
  public toogleOpen(): void {
    this.isOpen = !this.isOpen;
  }

  getErrorEmailMessage() {
    if (this.Email.hasError('required')) {
      return '';
    }

    return this.Email.hasError('email') ? 'Entrer un email Valide' : '';
  }

  getErrorMessageUsername() {
    if (this.username.length <= 4 && this.username.length !== 0) {
      return 'hello';
    }
    return ''
  }

  getErrorMessagePassword() {
    if (this.password.length <= 3 && this.password.length !== 0) {
      return 'hello';
    }
    return ''
  }

  getErrorMessagePhone() {
    if (this.phone.length <= 9 && this.phone.length !== 0) {
      return 'hello'
    }
    return ''
  }

  getErrorMessageAll() {
    if (this.phone.length === 0 || this.username.length === 0 || this.phone.length === 0 || this.password.length === 0) {
      return 'hello'
    }
    return''
  }

  public onClickRegister(): void {
    console.log({
      email: this.email,
      password: this.password,
    });
    this.userController
        .registerMerchant(this._loginService.loginInfos.email,
            this._loginService.loginInfos.password,
        )
        .subscribe({
              next: (res: any) => {
                this._loginService.modifyLoginInfos(res);
                this._loginService.modifyEmail(this.email);
                this._loginService.modifyPwd(this.password);
                this.connected = true;
                this._loginService.loginInfos.connected = true;
                console.log(this._loginService.getLoginInfos());
                /*this.userController.setToken(res.access_token);
                console.log(this.userController.token);
                console.log("connected");*/
              },
              error: (error) => {
                console.warn(error);
              },
        })};

  ngOnInit(): void {
    let infos = this._loginService.getLoginInfos();
    this.email = infos.email;
    this.password = infos.password;
  }
}

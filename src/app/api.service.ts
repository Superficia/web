import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {observable, Observable, of} from 'rxjs';
import {IRegister} from "./model/loginInfos";
import { Subscription } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    public token: string = "";
    private authHeaderForm = {'Content-Type': 'application/x-www-form-urlencoded'};
    private authHeaderJson: any = null;
    private _serverURL: string = "http://18.220.66.225:8080";
    public vendor_id = localStorage.getItem('vendor_id');

    constructor(private http: HttpClient) {
    }

    public login(email: string, password: string): Observable<any> {
        console.log(email, password);
        let body = new URLSearchParams();
        body.set('username', email);
        body.set('password', password);
        return this.http.post(
            `${this._serverURL}/login`,
            body,
            {headers: this.authHeaderForm}
        );
    }

    public setToken(token: string) {
        this.token = token;
        this.authHeaderJson = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`,
        });
    }

    public registerMerchant(email: string, password: string): Observable<any> {
        let body = new URLSearchParams();
        body.set('email', email);
        body.set('password', password);
        return this.http.post(
            `${this._serverURL}/user/register`,
            body,
            {headers: this.authHeaderForm}
        );
    }

    public get(): Observable<any> {
        return this.http.get<any>(`${this._serverURL}/user/get`, {headers: this.authHeaderJson})
    }

    public consumeCode(code: string, email: string): Observable<any> {
        return this.http.post<any>(`${this._serverURL}/code/consume/${code}/${email}`,
            {headers: this.authHeaderForm})
    }


    /* consumeCode(code: string): Observable<boolean>{
       return this.http.post(`${this._serverURL}/betacode`, { code});
     }*/

    public modifyEmail(newEmail: string) {
      let body = new URLSearchParams();
        body.set('newEmail', newEmail);
      return this.http.post(
        `${this._serverURL}/user/modify/email`,
        body,
        { headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `Bearer ${this.token}`,
        })}
      );
    }

    public modifyPassword(newPassword: string) {
      let body = new URLSearchParams();
        body.set('newPassword', newPassword);
      return this.http.post(
        `${this._serverURL}/user/modify/password`,
        body,
        { headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `Bearer ${this.token}`,
        }) }
      );
    }
    public contactUs(_subject: string, _email: string, _content: string) {
        return this.http.post(
            `${this._serverURL}/contactUs`, {subject: _subject, email: _email, content: _content}, {headers: this.authHeaderForm });
    }
}

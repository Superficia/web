import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ILoginInfos } from './model/loginInfos';
import { Router } from '@angular/router';
import {Observable, BehaviorSubject} from "rxjs";
import {ErrorHandler} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public email: string =  '';

  public loginInfos: ILoginInfos = {
    email: "",
    password: "",
    connected: false,
    betaAccess: false,
    verified: false,
    uid: "",
    country: "",
    lastname: "",
    firstname: "",
    company: "",
    role: "",
    subject: "",
    content: "",
  };

  constructor(private _apiService: ApiService, private router: Router, private httpClient : HttpClient) {

  }

  getLoginInfos(): ILoginInfos {
    return (this.loginInfos);
  }
  public token = localStorage.getItem('access_token');
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: `Bearer ${this.token}`,
  });


  disconnect() {
    this.loginInfos = {
      email: "",
      password: "",
      connected: false,
      betaAccess: false,
      verified: false,
      uid: "",
      country: "",
      lastname: "",
      firstname: "",
      company: "",
      role: "",
      subject: "",
      content: ""
    };
  }

  modifyLoginInfos(infos: ILoginInfos) {
    this.loginInfos = infos;
  }

  modifyEmail(email: string) {
    this.loginInfos.email = email;
  }

  modifyPwd(password: string) {
    this.loginInfos.password = password;
  }

  updateBetaAccess(access: boolean) {
    this.loginInfos.betaAccess = access;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ErrorHttpHandler implements ErrorHandler {
  constructor() {
  }

  private static isHttpErrorResponse(error: any): error is HttpErrorResponse {
    return error instanceof HttpErrorResponse;
  }

  public handleError(error: any): any {
    if (ErrorHttpHandler.isHttpErrorResponse(error)) {
      console.log(error);
      return new Observable(send => send.next(error));
    }
  }
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginResponse = new BehaviorSubject<ILoginInfos | null>(null);

  getAuthLogin(): Observable<ILoginInfos | null> {
    return this.loginResponse.asObservable();
  }
}

import { Component, OnInit } from '@angular/core';
import { trigeAnimation } from '../../shared/animation';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  animations: [trigeAnimation]
})
export class ProjectComponent implements OnInit {
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterBetaCodeComponent } from './enter-beta-code.component';

describe('EnterBetaCodeComponent', () => {
  let component: EnterBetaCodeComponent;
  let fixture: ComponentFixture<EnterBetaCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterBetaCodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterBetaCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { ApiService } from '../api.service';
import {trigAnimation} from '../../shared/animation';
//import { MessageService } from 'primeng/api';
//import { MessageApiResponse } from "../ResponsesMsgCodes";
import { Router } from '@angular/router';
import {
  AuthService,
  ErrorHttpHandler,
} from '../login.service';
import {Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-enter-beta-code',
  templateUrl: './enter-beta-code.component.html',
  styleUrls: ['./enter-beta-code.component.scss'],
  animations: [trigAnimation],
  //providers: [MessageService, ErrorHttpHandler]
})
export class EnterBetaCodeComponent implements OnInit {

  public betaAccess: boolean = false;
  public connected: boolean = false;
  public accessCode: string = "";
  isOpen = true;

  constructor(public _loginService: LoginService, private _apiService: ApiService, /*private messageService: MessageService, private router: Router,*/) {
  }

  ngOnInit(): void {

    let infos = this._loginService.getLoginInfos();
    this.betaAccess = infos.betaAccess;
    this.connected = infos.connected;
  }

  /*useCode(code: string): void {
    this._apiService.consumeCode(code).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Validation: ',
        detail: MessageApiResponse.french[res.message],
      });
      this.authHandler.nextAuthLogin(res);
      setTimeout(() => {
        this.router.navigate(['Home']);
      }, 2000);
      console.log('Data transmitted, res: ', res);
    }, err => {
      console.warn(err);
      this.messageService.add({
        severity: 'warn',
        summary: 'ERREUR: ',
        detail: MessageApiResponse.french[err.error.message],
      });
    })
  }*/
  useCode() {
    this._apiService.consumeCode(this.accessCode, this._loginService.loginInfos.email).subscribe( {
      next : (res: any) => {
        console.log(res);
        this.betaAccess = true;
      },
      error: (error) => {
        console.warn(error);
      }
    })
    this._loginService.updateBetaAccess(this.betaAccess);
  }
}




import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetaClosedComponent } from './beta-closed.component';

describe('BetaClosedComponent', () => {
  let component: BetaClosedComponent;
  let fixture: ComponentFixture<BetaClosedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetaClosedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetaClosedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

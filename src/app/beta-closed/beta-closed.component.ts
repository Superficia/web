import { Component, OnInit } from '@angular/core';
import {trigAnimation} from '../../shared/animation';

@Component({
  selector: 'app-beta-closed',
  templateUrl: './beta-closed.component.html',
  styleUrls: ['./beta-closed.component.scss'],
  animations: [trigAnimation]
})
export class BetaClosedComponent implements OnInit {
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
  }

}

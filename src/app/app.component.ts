import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './login.service';
import {MatTooltipModule} from '@angular/material/tooltip';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'superficia-web';

  public Menu = true;

  constructor(
      private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  isHomeRoute() {
    return this.router.url !== '/' && this.router.url !== '/#Superficia';
  }
  spacer() {
    return this.router.url === '/' || this.router.url === '/#Superficia';
  }
}
    /*
    console.log('hello')
    if (this.router.url != '/') {
      console.log(this.router.url);
      this.Menu = true;
    } else {
      console.log(this.router.url);
      this.Menu = false;
    }
  }
}*/

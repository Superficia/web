export interface IMembers {
    name: string,
    role: string,
    mail: string,
    linkedin: string,
}

export var members: IMembers[] = [
    {
        name: "Alexis Wirth",
        role: "CTO, Front Développeur",
        mail: "alexis.wirth@epitech.eu",
        linkedin: "https://google.fr",
    },
    {
        name: "Paolo Réant-Hamadi",
        role: "CEO, Back Développeur",
        mail: "paolo.reant-hamadi@epitech.eu",
        linkedin: "https://google.fr",
    },
    {
        name: "Florian Theron",
        role: "Developpeur Logiciel",
        mail: "florian.theron@epitech.eu",
        linkedin: "https://google.fr",
    },
    {
        name: "Yoan Vessiere",
        role: "Développeur Logiciel",
        mail: "yoan.vessiere@epitech.eu",
        linkedin: "https://google.fr",
    },
    {
        name: "Antoine Maillard",
        role: "Développeur Algorithm",
        mail: "antoine.maillard@epitech.eu",
        linkedin: "https://google.fr",
    },
    {
        name: "Théo Henry",
        role: "Front Développeur",
        mail: "theo.henry@epitech.eu",
        linkedin: "https://google.fr",
    }
];

export interface ContactUsModel {
  email: string;
  subject: string;
  content: string;
}

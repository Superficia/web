import {Observable} from "rxjs";

export interface ILoginInfos {
    email: string,
    password: string,
    connected: boolean,
    betaAccess: boolean,
    verified: boolean,
    uid: string,
    country: string,
    lastname: string,
    firstname: string,
    company: string,
    role: string,
    subject: string,
    content: string,
}

export interface IRegister {
    email:string;
    password: string;
}

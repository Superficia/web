import { Injectable } from '@angular/core';
import { IConfig } from './model/config';
import * as data from '../config/config.json';

@Injectable({
  providedIn: 'root'
})
export class ConfigLoaderService {

  private config: IConfig = {
    betaOpen: false
  };
  constructor() {
    this.config = data;
  }

  isBetaOpen(): boolean {
    return (this.config.betaOpen);
  }
}

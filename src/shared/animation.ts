import {style, animate, trigger, transition, state, keyframes} from '@angular/animations';

export const trigAnimation = trigger('flyOnOut', [
    state('bin', style({ transform: 'translateX(0)' })),
    state('bin', style({backgroundColor : 'transparent'})),
    transition('* => bin', [
      style({ transform: 'translateX(-200%)' }),
      style({backgroundColor : 'transparent' }),
      animate(250)
    ]),
    transition('bin => *', [
     animate(250, style({ transform: 'translateX(300%)' })),
      style({backgroundColor : 'transparent' }),
    ])
  ]);

export const trigeAnimation = trigger('flyOnOut', [
  state('bin', style({ transform: 'translateY(0)' })),
  state('bin', style({backgroundColor : 'transparent'})),
  transition('* => bin', [
    style({ transform: 'translateY(200%)' }),
    style({backgroundColor : 'white' }),
    animate(250)
  ]),
  transition('bin => *', [
    animate(250, style({ transform: 'translateY(-200%)' })),
    style({backgroundColor : 'white' }),
  ])
]);

export const triAnimation = trigger('flyOnOut', [
  state('bin', style({ transform: 'translateX(0)' })),
  state('bin', style({backgroundColor : 'transparent'})),
  transition('* => bin', [
    style({ transform: 'translateX(-100%)' }),
    style({backgroundColor : 'white' }),
  ]),
  transition('bin => *', [
    style({backgroundColor : 'white' }),
  ])
]);

export const loAnimation = trigger('openClose', [
  state('open', style({
    backgroundColor: 'transparent',
  })),
  state('closed', style({
    backgroundColor: 'transparent',
  })),
  transition('* => open' , [
    animate('1s', keyframes([
      style({backgroundColor: 'black' }),
      style({backgroundColor: '#eb9f9f'}),
      style({backgroundColor: '#F2BFBF'})
    ]))
  ]),
  transition('* => closed' , [
    animate('1s', keyframes ( [
      style({backgroundColor: 'black' }),
      style({backgroundColor: '#EB9F9F'}),
      style({backgroundColor: '#F2BFBF'})
    ]))
  ])
]);
